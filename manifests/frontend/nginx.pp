class routenbuch::frontend::nginx {
  $servername = $routenbuch::servername
  $local_port = $routenbuch::local_port

  letsencrypt::certonly { 'routenbuch':
    domains              => [ $servername ],
    manage_cron          => true,
    cron_before_command  => '/bin/systemctl stop nginx.service',
    cron_success_command => '/bin/systemctl reload-or-restart nginx.service',
    cron_output          => 'suppress',
    before               => Service['nginx'],
  }

  nginx::resource::server { "${servername}-redirect":
    listen_port  => 80,
    ipv6_enable  => true,
    ipv6_listen_options => '',
    server_name  => [$servername],
    ssl_redirect => true,
  }

  nginx::resource::server { $servername:
    listen_port         => 443,
    ipv6_enable         => true,
    ipv6_listen_options => '',
    proxy               => "http://127.0.0.1:${local_port}",
    ssl                 => true,
    ssl_cert            => "/etc/letsencrypt/live/routenbuch/fullchain.pem",
    ssl_key             => "/etc/letsencrypt/live/routenbuch/privkey.pem",
    ssl_port            => 443,
    proxy_set_header    => [
      'Host $host',
      'X-Real-IP $remote_addr',
      'X-Forwarded-For $proxy_add_x_forwarded_for',
      'X-Forwarded-Proto https',
      'Proxy ""',
    ],
    raw_append          =>  [
      'add_header Strict-Transport-Security "max-age=63072000" always;'
    ]
  }
}
