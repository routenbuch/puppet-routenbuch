# @api public
# @summary install and configure the routenbuch application
#
# @param db_password
#   Password for postgres database
# @param secret_key_base
#   Key to encrypt rails secrets
# @param ensure
#   Valid values are: 'present', 'absent'
# @param base_path
#   Routenbuch installation root path
# @param routenbuch_image
#   URL of the routenbuch docker image
# @param routenbuch_image_tag
#   Version tag of the routenbuch docker image to use
# @param redis_image
#   URL of the redis docker images
# @param redis_image_tag
#   Version tag of the redis docker image to use
# @param postgres_image
#   URL of the postgres docker image
# @param postgres_image_tag
#   Version tag of the postgres docker image to use
# @param frontend
#   Could be used to enable the configuration of a frontend webserver.
#
#   Valid values are:
#   * `none` - No frontend server configuration
#   * `traefik` - Configure for traefik frontend server
#   * `nginx` - Configure a virtual host for nginx
# @param servername
#   Hostname of the routenbuch instance.
class routenbuch (
  String $servername,
  String $db_password,
  String $secret_key_base,
  Enum['present', 'absent'] $ensure = 'present',
  String $base_path = '/opt/routenbuch',
  String $routenbuch_image = 'registry.gitlab.com/routenbuch/routenbuch',
  String $routenbuch_image_tag = 'latest',
  String $redis_image = 'redis',
  String $redis_image_tag = '6',
  String $postgres_image = 'postgres',
  String $postgres_image_tag = '13',
  Enum['none', 'traefik', 'nginx'] $frontend = 'none',
  Integer[1025,50000] $local_port = 8080,
) {
  $redis_data_path = "${base_path}/redis-data"
  $pgdata_path = "${base_path}/pgdata"
  $data_path = "${base_path}/data"

  if($frontend == 'nginx') {
    include routenbuch::frontend::nginx
  }

  file { [ $base_path, $pgdata_path, $data_path, $redis_data_path]:
    ensure => 'directory',
    before => [
      Docker::Run['db-routenbuch', 'web-routenbuch', 'redis-routenbuch'],
    ],
  }

  $rails_env = [
    'RAILS_ENV=production',
    'RAILS_SERVE_STATIC_FILES=true',
    'RAILS_LOG_TO_STDOUT=true',
    'DB_HOST=database',
    'DB_DATABASE=routenbuch',
    'DB_USER=routenbuch',
    "DB_PASSWORD=${db_password}",
    "SECRET_KEY_BASE=${secret_key_base}",
    'REDIS_URL=redis://redis:6379',
  ]

  if($frontend == 'traefik') {
    $labels = [
      'traefik.enable=true',
      'traefik.backend=web-routenbuch',
      'traefik.port=3000',
      'traefik.frontend.passHostHeader=true',
      "traefik.frontend.rule=Host:${servername}",
    ]
  } else {
    $web_labels = []
  }

  docker::run { 'web-routenbuch':
    ensure          => $ensure,
    image           => "${routenbuch_image}:${routenbuch_image_tag}",
    restart_service => true,
    labels          => $web_labels,
    ports           => [
      "127.0.0.1:${local_port}:3000"
    ],
    volumes         => [
      "${data_path}:/data",
    ],
    env             => $rails_env,
    links           => [
      'db-routenbuch:database',
      'redis-routenbuch:redis'
    ],
    privileged      => false,
    pull_on_start   => true,
  }
  docker::run { 'sidekiq-routenbuch':
    ensure           => $ensure,
    image            => "${routenbuch_image}:${routenbuch_image_tag}",
    command          => 'bundle exec sidekiq',
    restart_service  => true,
    ports            => [ ],
    volumes          => [
      "${data_path}:/data",
    ],
    env              => $rails_env,
    links            => [
      'db-routenbuch:database',
      'redis-routenbuch:redis'
    ],
    privileged       => false,
    pull_on_start    => true,
    extra_parameters => [ '--no-healthcheck' ],
  }
  docker::run { 'redis-routenbuch':
    ensure          => $ensure,
    image           => "${redis_image}:${redis_image_tag}",
    command         => 'redis-server --appendonly yes',
    restart_service => true,
    ports           => [ ],
    volumes         => [
      "${redis_data_path}:/data",
    ],
    links           => [ ],
    privileged      => false,
    pull_on_start   => true,
  }
  docker::run { 'db-routenbuch':
    image           => "${postgres_image}:${postgres_image_tag}",
    restart_service => true,
    labels          => [ ],
    ports           => [ ],
    volumes         => [
      "${pgdata_path}:/var/lib/postgresql/data/pgdata"
    ],
    env             => [
      'POSTGRES_USER=routenbuch',
      "POSTGRES_PASSWORD=${db_password}",
      'PGDATA=/var/lib/postgresql/data/pgdata',
    ],
    links           => [ ],
    privileged      => false,
    pull_on_start   => true,
  }
}
