# routenbuch

## Module description

Routenbuch is a climbing management application written using the Ruby on Rails framework.
This puppet module simplifies the installation and configuration of routenbuch instances.

## Setup

### What the routenbuch module affects:

* Creates configuration and file system hierarchy (by default `/opt/routenbuch`)
* Setup of routenbuch application using docker containers
* Setup of postgres and redis using docker containers

### Beginning with routenbuch

To setup a basic routenbuch installation with a Apache frontend webserver:

```
class { 'apache':
  default_vhost => false
}
class { 'docker': }

class { 'routenbuch':
  secret_key_base => '<key>',
  db_password => '<secret>',
  frontend => 'apache',
  servername => 'routenbuch.markusbenning.de'
}
```

## Reference

For information on classes, types and functions see the [REFERENCE.md](https://gitlab.com/routenbuch/puppet-routenbuch/-/blob/master/REFERENCE.md)

