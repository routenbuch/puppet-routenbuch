require 'spec_helper'

describe 'routenbuch' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) do
        os_facts
      end

      let(:pre_condition) { 'include docker' }

      let(:params) do
        {
          'db_password' => 'secret',
          'secret_key_base' => '123abc'
        }
      end

      it { is_expected.to compile.with_all_deps }

      context 'with default values for all parameters' do
        it 'main classes' do
          is_expected.to contain_class 'routenbuch'
        end

        it 'containers' do
          is_expected.to contain_docker__run('db-routenbuch')
          is_expected.to contain_docker__run('redis-routenbuch')
          is_expected.to contain_docker__run('sidekiq-routenbuch')
          is_expected.to contain_docker__run('web-routenbuch')
        end

        it 'directories' do
          is_expected.to contain_file('/opt/routenbuch/data')
          is_expected.to contain_file('/opt/routenbuch/pgdata')
          is_expected.to contain_file('/opt/routenbuch/redis-data')
          is_expected.to contain_file('/opt/routenbuch')
        end
      end
    end
  end
end
